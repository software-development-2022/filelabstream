/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pinkz7_
 */
public class ReadPlayer {
    public static void main(String[] args) {
        Player o = null;
        Player x = null;
        FileInputStream fis = null;
        try {
            File file = new File("players.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            o = (Player) ois.readObject();
            x = (Player) ois.readObject();
            
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            o = new Player('o');
            x = new Player('x');
        } catch (IOException ex) {
            Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(fis!=null){
                    fis.close();
                }

            } catch (IOException ex) {
                Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println(o);
        System.out.println(x);
    }
}
